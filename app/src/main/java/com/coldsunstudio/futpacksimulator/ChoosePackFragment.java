package com.coldsunstudio.futpacksimulator;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import java.util.List;

public class ChoosePackFragment extends Fragment implements View.OnClickListener {

    FrameLayout blackScreen;
    ProgressBar spinner;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_choose_pack, container, false);
        blackScreen = (FrameLayout) rootView.findViewById(R.id.blackscreen);
        spinner = (ProgressBar) rootView.findViewById(R.id.progress_spinner);

        List<View> buttons = rootView.getTouchables();
        for(View button : buttons) {
            button.setOnClickListener(ChoosePackFragment.this);
        }

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        blackScreen.setVisibility(View.INVISIBLE);
        spinner.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onClick(View view) {

        blackScreen.setVisibility(View.VISIBLE);
        spinner.setVisibility(View.VISIBLE);

        final String viewTag = (String) view.getTag();
        if(viewTag == null)
            return;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                final Intent openingActivityIntent = new Intent(ChoosePackFragment.this.getContext(), OpeningActivity.class);
                openingActivityIntent.putExtra("CHOSEN_PACK_TAG", viewTag);
                startActivity(openingActivityIntent);
            }
        }, 1000);

    }
}
