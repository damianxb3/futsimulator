package com.coldsunstudio.futpacksimulator;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.coldsunstudio.futpacksimulator.data.Card;
import com.coldsunstudio.futpacksimulator.data.FUTCardAdapter;
import com.coldsunstudio.futpacksimulator.data.UserDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MyCardsFragment extends Fragment {

    Context mContext;
    GridView myCardsGridView;
    FrameLayout blackScreen;
    ProgressBar spinner;

    Card.Color cardsToShowColor = Card.Color.GOLD;
    private boolean isLoading;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContext = getContext();
        View view = inflater.inflate(R.layout.fragment_mycards, container, false);
        blackScreen = (FrameLayout) view.findViewById(R.id.mycards_blackscreen);
        spinner = (ProgressBar) view.findViewById(R.id.progress_spinner);
        myCardsGridView = (GridView) view.findViewById(R.id.mycards_gridview);
        myCardsGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, View view, final int position, long id) {
                if(isLoading)
                    return;
                blackScreen.setVisibility(View.VISIBLE);
                Card selectedCard = (Card)(parent.getAdapter()).getItem(position);

                CardViewBuilder cardViewBuilder = new CardViewBuilder(getContext(), selectedCard, 200);
                final View cardView = cardViewBuilder.build();
                blackScreen.addView(cardView);
                blackScreen.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(isLoading)
                            return;
                        blackScreen.removeView(cardView);
                        blackScreen.setVisibility(View.INVISIBLE);
                    }
                });
            }
        });

        setupMyCardsGridView();
        return view;
    }

    private void setupMyCardsGridView() {
        new AsyncTask<Card.Color, Void, List<Card>>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                isLoading = true;
                blackScreen.setVisibility(View.VISIBLE);
                spinner.setVisibility(View.VISIBLE);
            }

            @Override
            protected List<Card> doInBackground(Card.Color... params) {
                try {
                    Thread.currentThread();
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                UserDatabase dbHandler = new UserDatabase(mContext);
                List<Card> allUserCards = dbHandler.getAllCards();
                dbHandler.close();
                List<Card> cardsToShow = new ArrayList<>();
                for(Card card : allUserCards) {
                    if(card.getColor() == params[0])
                        cardsToShow.add(card);
                }
                Collections.sort(cardsToShow, new Comparator<Card>() {
                    @Override
                    public int compare(Card lhs, Card rhs) {
                        return rhs.getRank() - lhs.getRank();
                    }
                });
                return cardsToShow;
            }

            @Override
            protected void onPostExecute(List<Card> cardsToShow) {
                super.onPostExecute(cardsToShow);
                myCardsGridView.setAdapter(new FUTCardAdapter(mContext, cardsToShow));
                blackScreen.setVisibility(View.INVISIBLE);
                spinner.setVisibility(View.GONE);
                isLoading = false;
            }
        }.execute(cardsToShowColor);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    public void sortCards(View view) {
        String sortStr = ((Button) view).getText().toString();
        switch (sortStr) {
            case "GOLD":
                cardsToShowColor = Card.Color.GOLD;
                break;
            case "SILVER":
                cardsToShowColor = Card.Color.SILVER;
                break;
            case "BRONZE":
                cardsToShowColor = Card.Color.BRONZE;
                break;
        }
        setupMyCardsGridView();
        view.setEnabled(false);
        ViewGroup parent = (ViewGroup) view.getParent();
        View child;
        for (int i = 0; i < parent.getChildCount(); ++i) {
            child = parent.getChildAt(i);
            if (!child.equals(view))
                child.setEnabled(true);
        }

    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
