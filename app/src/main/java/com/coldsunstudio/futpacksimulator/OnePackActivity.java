package com.coldsunstudio.futpacksimulator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;

import com.coldsunstudio.futpacksimulator.data.Card;
import com.coldsunstudio.futpacksimulator.data.FUTCardAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class OnePackActivity extends AppCompatActivity {

    Button returnButton;
    FrameLayout blackScreen;
    GridView myCardsGridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_pack);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ArrayList<Card> receivedCardsPack = (ArrayList<Card>) getIntent().getSerializableExtra("CardsObject");

        myCardsGridView = (GridView) findViewById(R.id.mycards_gridview);
        blackScreen = (FrameLayout) findViewById(R.id.mycards_blackscreen);
        returnButton = (Button) findViewById(R.id.btn_mainactivity_return);


        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OnePackActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        Collections.sort(receivedCardsPack, new Comparator<Card>() {
            @Override
            public int compare(Card lhs, Card rhs) {
                return rhs.getRank() - lhs.getRank();
            }
        });

        final FUTCardAdapter futCardAdapter = new FUTCardAdapter(this, receivedCardsPack);
        myCardsGridView.setAdapter(futCardAdapter);
        myCardsGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, View view, final int position, long id) {
                returnButton.setVisibility(View.INVISIBLE);
                blackScreen.setVisibility(View.VISIBLE);
                Card selectedCard = (Card) futCardAdapter.getItem(position);
                CardViewBuilder cardViewBuilder =
                        new CardViewBuilder(OnePackActivity.this, selectedCard, 200);
                final View cardView = cardViewBuilder.build();
                blackScreen.addView(cardView);

                blackScreen.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        blackScreen.removeView(cardView);
                        returnButton.setVisibility(View.VISIBLE);
                        blackScreen.setVisibility(View.INVISIBLE);
                    }
                });
            }
        });
    }

}
