package com.coldsunstudio.futpacksimulator;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.coldsunstudio.futpacksimulator.data.Card;
import com.squareup.picasso.Picasso;

public class CardViewBuilder {

    private Context context;
    private Card card;
    private int width;
    private int height;


    public CardViewBuilder(Context context, Card card, int width) {
        this.context = context;
        this.card = card;
        this.width = getPixels(width);
        this.height = getPixels(width*3/2);
    }


    public View build() {
        FrameLayout wrapper = new FrameLayout(context);
        RelativeLayout cardView = new RelativeLayout(context);
        cardView.setLayoutParams(new FrameLayout.LayoutParams(width, height,
                Gravity.CENTER));
        wrapper.addView(cardView);

        // font colors:
        @ColorRes int textColors[] = Card.getTextColors(card.getBackgroundFileName());

        // background
        String backgroundFileName = card.getBackgroundFileName();
        ImageView backgroundImageView = new ImageView(context);
        backgroundImageView.setId(R.id.card_view_background);
        backgroundImageView.setLayoutParams(new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        Picasso.with(context)
                .load("file:///android_asset/backgrounds/bg-" + backgroundFileName + ".png")
                .into(backgroundImageView);
        backgroundImageView.setContentDescription("Background");
        cardView.addView(backgroundImageView);

        // picture
        ImageView pictureImageView = new ImageView(context);
        pictureImageView.setId(R.id.card_view_picture);
        RelativeLayout.LayoutParams pictureLayoutParams = new RelativeLayout.LayoutParams(
                (int)(0.55*width), (int)(0.4*height));
        pictureLayoutParams.topMargin = (int)(0.12*height);
        pictureLayoutParams.leftMargin = (int)(0.37*width);
        pictureImageView.setLayoutParams(pictureLayoutParams);
        Picasso.with(context)
                .load("file:///android_asset/pictures/" + card.getPictureFileName() + ".png")
                .placeholder(R.drawable.picture_placeholder)
                .into(pictureImageView);
        cardView.addView(pictureImageView);

        // name:
        TextView nameTextView = new TextView(context);
        nameTextView.setId(R.id.card_view_name);
        RelativeLayout.LayoutParams nameLayoutParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        nameLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        nameLayoutParams.topMargin = (int)(0.5*height);
        nameLayoutParams.leftMargin = (int)(0.046*width);
        nameTextView.setLayoutParams(nameLayoutParams);
        if(card.getName().length() > 15)
            nameTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int)(0.05*height));
        else
            nameTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int)(0.06*height));
        nameTextView.setText(card.getName());
        nameTextView.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        nameTextView.setTextColor(ContextCompat.getColor(context,
                backgroundFileName.contains("tots") ? android.R.color.black : textColors[1]
        ));
        cardView.addView(nameTextView);

        // rank:
        TextView rankTextView = new TextView(context);
        rankTextView.setId(R.id.card_view_rank);
        RelativeLayout.LayoutParams rankLayoutParams = new RelativeLayout.LayoutParams(
                (int)(0.5*width), ViewGroup.LayoutParams.WRAP_CONTENT);
        rankTextView.setGravity(Gravity.CENTER);
        rankLayoutParams.topMargin = (int)(0.085*height);
        rankLayoutParams.leftMargin = (int)(0.046*width);
        rankTextView.setLayoutParams(rankLayoutParams);
        rankTextView.setText(String.valueOf(card.getRank()));
        rankTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int)(0.1*height));
        rankTextView.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        rankTextView.setTextColor(ContextCompat.getColor(context, textColors[0]));
        cardView.addView(rankTextView);

        // position:
        TextView positionTextView = new TextView(context);
        positionTextView.setId(R.id.card_view_position);
        RelativeLayout.LayoutParams positionLayoutParams = new RelativeLayout.LayoutParams(
                (int)(0.5*width), ViewGroup.LayoutParams.WRAP_CONTENT);
        positionTextView.setGravity(Gravity.CENTER);
        positionLayoutParams.topMargin = (int)(0.19*height);
        positionLayoutParams.leftMargin = (int)(0.046*width);
        positionTextView.setLayoutParams(positionLayoutParams);
        positionTextView.setText(card.getPosition());
        positionTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int)(0.07*height));
        positionTextView.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        positionTextView.setTextColor(ContextCompat.getColor(context, textColors[0]));
        cardView.addView(positionTextView);

        // nation:
        ImageView nationImageView = new ImageView(context);
        nationImageView.setId(R.id.card_view_nation);
        RelativeLayout.LayoutParams nationLayoutParams = new RelativeLayout.LayoutParams(
                (int)(0.223*width), (int)(0.145*height));
        nationLayoutParams.topMargin = (int)(0.37*height);
        nationLayoutParams.leftMargin = (int)(0.177*width);
        nationImageView.setLayoutParams(nationLayoutParams);
        Picasso.with(context)
                .load("file:///android_asset/nations/" + card.getNationalityId() + ".png")
                .placeholder(R.drawable.nation_placeholder)
                .into(nationImageView);
        nationImageView.setContentDescription("Nation");
        cardView.addView(nationImageView);

        // attributes:
        int[] cardAttributes = card.getAttributes();
        final String[] attrNames = {"PAC", "SHO", "PAS", "DRI", "DEF", "PHY"};
        final int[] attrTopMargins = {(int)(0.57*height), (int)(0.65*height), (int)(0.73*height)};
        final int[] attrLeftMargins = {(int)(0.154*width), (int)(0.57*width)};

        for(int i = 0; i < 6; i++) {
            TextView attrTextView = new TextView(context);
            attrTextView.setId(7 + i);
            RelativeLayout.LayoutParams attrLayoutParams = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            attrLayoutParams.topMargin = attrTopMargins[i%3];
            attrLayoutParams.leftMargin = attrLeftMargins[i/3];
            attrTextView.setLayoutParams(attrLayoutParams);
            String attrText = "<b>" + attrNames[i] + "</b> " + cardAttributes[i];
            attrTextView.setText(Html.fromHtml(attrText));
            attrTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int)(0.06*height));
            attrTextView.setTextColor(ContextCompat.getColor(context, textColors[1]));
            cardView.addView(attrTextView);
        }

        return wrapper;
    }




    private int getPixels(int dipValue){
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue,
                context.getResources().getDisplayMetrics());
    }


}
