package com.coldsunstudio.futpacksimulator;

import android.util.Log;

import com.coldsunstudio.futpacksimulator.data.Card;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;


public class CardRandomizer {

    final static String LOG_TAG = CardRandomizer.class.getSimpleName();
    final static double DEFAULT_FACTOR = 0.8;

    private List<Card> allAvailableCards;
    private double factor;


    public CardRandomizer(List<Card> allAvailableCards, double factor) {
        this.allAvailableCards = allAvailableCards;
        if(factor >= 1.0)
            factor = DEFAULT_FACTOR;
        this.factor = factor;
    }

    public ArrayList<Card> getCardsPack(Card.CardPack packType) {

        Card.Color cardColor = Card.Color.GOLD;
        int cardsNumber = 12;
        int rareCardsNumber = 0;
        ArrayList<Card> cardsPack = new ArrayList<>(cardsNumber);

        // premium gold mPack:
        switch(packType) {
            case PREMIUM_GOLD:
                cardColor = Card.Color.GOLD;
                rareCardsNumber = 3;
                break;
            case GOLD:
                cardColor = Card.Color.GOLD;
                rareCardsNumber = 1;
                break;
            case PREMIUM_SILVER:
                cardColor = Card.Color.SILVER;
                rareCardsNumber = 3;
                break;
            case SILVER:
                cardColor = Card.Color.SILVER;
                rareCardsNumber = 1;
                break;
            case PREMIUM_BRONZE:
                cardColor = Card.Color.BRONZE;
                rareCardsNumber = 3;
                break;
            case BRONZE:
                cardColor = Card.Color.BRONZE;
                rareCardsNumber = 1;
                break;
        }

        // create banks of rare & nonrare cards of given color:
        List<Card> rareCardsBank = new ArrayList<>();
        List<Card> nonrareCardsBank = new ArrayList<>();
        for(Card card : allAvailableCards) {
            if(card.getColor() == cardColor) {
                if(card.isRare())
                    rareCardsBank.add(card);
                else
                    nonrareCardsBank.add(card);
            }
        }
        // sort the banks from the highest rank to the lowest one:
        Collections.sort(rareCardsBank, new Comparator<Card>() {
            @Override
            public int compare(Card lhs, Card rhs) {
                return rhs.getRank() - lhs.getRank();
            }
        });
        Collections.sort(nonrareCardsBank, new Comparator<Card>() {
            @Override
            public int compare(Card lhs, Card rhs) {
                return rhs.getRank() - lhs.getRank();
            }
        });

        // get rare cards:
        for(int i = 0; i < rareCardsNumber; ++i)
            cardsPack.add(getCard(
                    rareCardsBank,
                    rareCardsBank.get(rareCardsBank.size()-1).getRank(),
                    rareCardsBank.get(0).getRank()
            ));

        // get non-rare cards:
        for(int i = 0; i < cardsNumber-rareCardsNumber; ++i)
            cardsPack.add(getCard(
                    nonrareCardsBank,
                    nonrareCardsBank.get(nonrareCardsBank.size()-1).getRank(),
                    nonrareCardsBank.get(0).getRank()
            ));

        return cardsPack;
    }

    /**
     *
     * @param cardsBank - bank of cards admitted to the lottery
     * @param minRank - minimal rating of cards in the bank
     * @param maxRank - maximal rating of cards in the bank
     * @return - a random card from the bank
     */
    private Card getCard(List<Card> cardsBank, int minRank, int maxRank) {

        Card receivedCard;

        // draw a rating and create a list of cards with this rating:
        int randomValue = new Random().nextInt(Integer.MAX_VALUE);
        int randomRating = getCardRatingFromRandomValue(randomValue,
                Integer.MAX_VALUE, minRank, maxRank);
        List<Card> cardsWithRandomRating = new ArrayList<>();
        for(Card card : cardsBank)  {
            if(card.getRank() == randomRating)
                cardsWithRandomRating.add(card);
        }
        if(cardsWithRandomRating.isEmpty())
            Log.e(LOG_TAG, "getCard: incorrect ranks range.");

        // get a specific card from the list:
        randomValue = new Random().nextInt(cardsWithRandomRating.size());
        receivedCard = cardsWithRandomRating.get(randomValue);

        return receivedCard;
    }

    /**
     *
     * @param randomValue - a result of lottery
     * @param allValues - how many values were used in the lottery
     * @param minRank - minimal rating of the cards
     * @param maxRank - maximal rating of the cards
     * @return - rating of a card
     */
    private int getCardRatingFromRandomValue(int randomValue, int allValues,
                                             int minRank, int maxRank) {

        int cardsNumber = maxRank - minRank + 1;
        double[] probabilities = new double[cardsNumber];
        double firstProbabilityValue = 100d*(1d-factor)/(1d - Math.pow(factor, cardsNumber));

        // set probabilities:
        for (int i = 0; i < cardsNumber; ++i) {
            probabilities[i] = firstProbabilityValue * Math.pow(factor, i);
        }

        // get the card:
        int limitValue;
        for (int i = 0; i < cardsNumber; ++i) {
            limitValue = (int) (probabilities[i] / 100.0 * allValues);
            if (randomValue < limitValue)
                return i + minRank;
            else
                randomValue -= limitValue;
        }

        return minRank + cardsNumber;

    }


    /**
     * Debug function, delete it later:
     */

    public void printTestReceivedRating(List<Integer> allRatingsList, int lots, int rankMin, int rankMax) {
        for (int i = 0; i < lots; ++i) {
            Random rand = new Random();
            int allValues = Integer.MAX_VALUE;
            int randomValue = rand.nextInt(allValues);
            int receivedRating = getCardRatingFromRandomValue(randomValue, allValues, rankMin, rankMax);
            allRatingsList.add(receivedRating);
        }
    }
}
