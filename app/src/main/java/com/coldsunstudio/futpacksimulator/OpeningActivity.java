package com.coldsunstudio.futpacksimulator;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.coldsunstudio.futpacksimulator.data.Card;
import com.coldsunstudio.futpacksimulator.data.FUTDatabase;
import com.coldsunstudio.futpacksimulator.data.UserDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class OpeningActivity extends AppCompatActivity {

    private Card.CardPack mPack;
    private ImageView mPackToOpen;
    private Button mShowPackBtn;
    private FrameLayout mOneCard;
    private AnimationDrawable mCardAnimation;
    private ArrayList<Card> mReceivedCards;

    private boolean mIsAnimationDone;
    private boolean mIsCardPackLoaded;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opening);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mOneCard = (FrameLayout) findViewById(R.id.one_card);
        mPackToOpen = (ImageView) findViewById(R.id.pack_to_open);

        setupPackType();
        mCardAnimation = (AnimationDrawable) mPackToOpen.getBackground();
        mPackToOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPackToOpen.setOnClickListener(null);
                mCardAnimation.start();
                checkIfAnimationDone(mCardAnimation);
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        mReceivedCards = getRandomPack();
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        int width = (int) (mPackToOpen.getMeasuredWidth() / 2.5 / getResources().getDisplayMetrics().density);
                        View bestCard = new CardViewBuilder(
                                getApplicationContext(), mReceivedCards.get(0), width).build();
                        mOneCard.addView(bestCard);
                        if(mIsAnimationDone) {
                            mShowPackBtn.setVisibility(View.VISIBLE);
                            mIsAnimationDone = false;
                        } else
                            mIsCardPackLoaded = true;
                    }

                }.execute();

            }
        });

        mShowPackBtn = (Button) findViewById(R.id.showPackBtn);
        mShowPackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OpeningActivity.this, OnePackActivity.class);
                intent.putExtra("CardsObject", mReceivedCards);
                startActivity(intent);
            }
        });


    }

    private void setupPackType() {
        String chosenPackTag = getIntent().getStringExtra("CHOSEN_PACK_TAG");
        Log.v("OpeningActivity", chosenPackTag);
        switch (chosenPackTag) {
            case "premium_gold":
                mPack = Card.CardPack.PREMIUM_GOLD;
                mPackToOpen.setBackgroundResource(R.drawable.gold_pack_animation);
                break;
            case "premium_silver":
                mPack = Card.CardPack.PREMIUM_SILVER;
                mPackToOpen.setBackgroundResource(R.drawable.silver_pack_animation);
                break;
            case "premium_bronze":
                mPack = Card.CardPack.PREMIUM_BRONZE;
                mPackToOpen.setBackgroundResource(R.drawable.bronze_pack_animation);
                break;
            case "gold":
                mPack = Card.CardPack.GOLD;
                mPackToOpen.setBackgroundResource(R.drawable.gold_pack_animation);
                break;
            case "silver":
                mPack = Card.CardPack.SILVER;
                mPackToOpen.setBackgroundResource(R.drawable.silver_pack_animation);
                break;
            case "bronze":
                mPack = Card.CardPack.BRONZE;
                mPackToOpen.setBackgroundResource(R.drawable.bronze_pack_animation);
                break;
        }
        char first = Character.toUpperCase(chosenPackTag.charAt(0));
        setTitle(first + chosenPackTag.replace("_", " ").substring(1) + " pack");
    }

    private ArrayList<Card> getRandomPack() {

        CardRandomizer packRandomizer;
        FUTDatabase dbHandler = new FUTDatabase(this);

        if(mPack == Card.CardPack.PREMIUM_GOLD || mPack == Card.CardPack.GOLD)
            packRandomizer = new CardRandomizer(dbHandler.getAllGoldCards(), 14/23d);
        else if(mPack == Card.CardPack.PREMIUM_SILVER || mPack == Card.CardPack.SILVER)
            packRandomizer = new CardRandomizer(dbHandler.getAllSilverCards(), 21/23d);
        else //if(mPack == Card.CardPack.PREMIUM_BRONZE || mPack == Card.CardPack.BRONZE)
            packRandomizer = new CardRandomizer(dbHandler.getAllBronzeCards(), 22/23d);

        dbHandler.close();

        ArrayList<Card> receivedCards = packRandomizer.getCardsPack(mPack);
        Collections.sort(receivedCards, new Comparator<Card>() {
            @Override
            public int compare(Card lhs, Card rhs) {
                return rhs.getRank() - lhs.getRank();
            }
        });

        UserDatabase userCardsDb = new UserDatabase(this);
        userCardsDb.insertCards(receivedCards);
        userCardsDb.close();

        return receivedCards;

    }

    private void checkIfAnimationDone(AnimationDrawable anim){
        final AnimationDrawable a = anim;
        int timeBetweenChecks = 100;
        Handler h = new Handler();
        h.postDelayed(new Runnable(){
            public void run(){
                if (a.getCurrent() != a.getFrame(a.getNumberOfFrames() - 1)){
                    checkIfAnimationDone(a);
                } else {
                    if(mIsCardPackLoaded) {
                        mShowPackBtn.setVisibility(View.VISIBLE);
                        mIsAnimationDone = false;
                    }
                }
            }
        }, timeBetweenChecks);
    }

}
