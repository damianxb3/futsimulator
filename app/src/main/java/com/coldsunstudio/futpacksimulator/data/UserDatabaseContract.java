package com.coldsunstudio.futpacksimulator.data;

import android.provider.BaseColumns;

public final class UserDatabaseContract {

    public UserDatabaseContract() {}

    public static abstract class CardEntry implements BaseColumns {

        public static final String TABLE_NAME = "cards";
        public static final String COLUMN_NAME_ISNEW = "is_new";
        public static final String COLUMN_NAME_RANK = "rank";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_BACKGROUND = "background";
        public static final String COLUMN_NAME_POSITION = "position";
        public static final String COLUMN_NAME_NATIONALITYID = "nationality_id";
        public static final String COLUMN_NAME_CLUBID = "club_id";
        public static final String COLUMN_NAME_PICTUREPATH = "picture_path";
        public static final String COLUMN_NAME_ATTR1 = "attr_pac";
        public static final String COLUMN_NAME_ATTR2 = "attr_sho";
        public static final String COLUMN_NAME_ATTR3 = "attr_pas";
        public static final String COLUMN_NAME_ATTR4 = "attr_dri";
        public static final String COLUMN_NAME_ATTR5 = "attr_def";
        public static final String COLUMN_NAME_ATTR6 = "attr_phy";

    }

}
