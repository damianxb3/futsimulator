package com.coldsunstudio.futpacksimulator.data;

import android.content.Context;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.coldsunstudio.futpacksimulator.CardViewBuilder;
import com.coldsunstudio.futpacksimulator.R;
import com.squareup.picasso.Picasso;


import java.util.List;

public class FUTCardAdapter extends BaseAdapter {

    private Context mContext;
    private List<Card> mCards;

    public FUTCardAdapter(Context context, List<Card> cards) {
        mContext = context;
        mCards = cards;
    }

    @Override
    public int getCount() {
        return mCards.size();
    }

    @Override
    public Object getItem(int position) {
        return mCards.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Card card = mCards.get(position);
        @ColorRes int textColors[] = Card.getTextColors(card.getBackgroundFileName());

        View view = convertView;
        CardHolder holder;

        if(view == null) {
            view = new CardViewBuilder(mContext, card, 70).build();
            holder = new CardHolder(view);
            view.setTag(holder);
        } else
            holder = (CardHolder) view.getTag();

        // background
        String backgroundFileName = card.getBackgroundFileName();
        Picasso.with(mContext)
                .load("file:///android_asset/backgrounds/bg-" + backgroundFileName + ".png")
                .into(holder.background);

        // picture
        Picasso.with(mContext)
                .load("file:///android_asset/pictures/" + card.getPictureFileName() + ".png")
                .placeholder(R.drawable.picture_placeholder)
                .into(holder.picture);

        // name
        holder.name.setText(card.getName());
        if(card.getName().length() > 15)
            holder.name.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int)(holder.name.getTextSize() * 0.8333f));
        if(backgroundFileName.contains("tots"))
            holder.name.setTextColor(ContextCompat.getColor(mContext, android.R.color.black));
        else
            holder.name.setTextColor(ContextCompat.getColor(mContext, textColors[1]));

        // rank
        holder.rank.setText(String.valueOf(card.getRank()));
        holder.rank.setTextColor(ContextCompat.getColor(mContext, textColors[0]));

        // position
        holder.position.setText(card.getPosition());
        holder.position.setTextColor(ContextCompat.getColor(mContext, textColors[0]));

        // nation
        Picasso.with(mContext)
                .load("file:///android_asset/nations/" + card.getNationalityId() + ".png")
                .placeholder(R.drawable.nation_placeholder)
                .into(holder.nation);

        // attributes
        int[] attr = card.getAttributes();
        final String[] attrNames = {"PAC", "SHO", "PAS", "DRI", "DEF", "PHY"};
        for(int i = 0; i < 6; i++) {
            String attrText = attrNames[i] + " " + attr[i];
            holder.attributes[i].setText(attrText);
            holder.attributes[i].setTextColor(ContextCompat.getColor(mContext, textColors[1]));
        }

        return view;
    }

    static class CardHolder {
        ImageView background, picture, nation;
        TextView name, rank, position;
        TextView[] attributes = new TextView[6];
        public CardHolder(View view) {
            background = (ImageView) view.findViewById(R.id.card_view_background);
            picture = (ImageView) view.findViewById(R.id.card_view_picture);
            nation = (ImageView) view.findViewById(R.id.card_view_nation);
            name = (TextView)view.findViewById(R.id.card_view_name);
            rank = (TextView)view.findViewById(R.id.card_view_rank);
            position = (TextView)view.findViewById(R.id.card_view_position);
            for(int i = 0; i < 6; i++)
                attributes[i] = ((TextView)view.findViewById(7 + i));
        }
    }

}
