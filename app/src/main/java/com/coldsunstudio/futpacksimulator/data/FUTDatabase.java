package com.coldsunstudio.futpacksimulator.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is responsible for storing all cards available in the app,
 */

public class FUTDatabase extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "futcards.sqlite";
    private static final int DATABASE_VERSION = 1;
    private static final String CARDS_TABLE_NAME = "cards";
    final String[] SELECT_ALL_CARD_FIELDS = {"0 _id", "rank", "name", "background",
            "position", "nationality_id", "club_id", "picture_path",
            "attr_pac", "attr_sho", "attr_pas", "attr_dri", "attr_def", "attr_phy"};

    public FUTDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    public List<Card> getAllGoldCards() {
        return getCardsQuery(SELECT_ALL_CARD_FIELDS, "rank >= 75", null, null, null, null);
    }

    public List<Card> getAllSilverCards() {
        return getCardsQuery(SELECT_ALL_CARD_FIELDS, "rank < 75 AND rank >= 65", null, null, null, null);
    }

    public List<Card> getAllBronzeCards() {
        return getCardsQuery(SELECT_ALL_CARD_FIELDS, "rank < 65", null, null, null, null);
    }

    public List<Card> getCardsQuery(String[] projectionIn, String selection,
                                    String[] selectionArgs, String groupBy,
                                    String having, String sortOrder) {

        List<Card> cardsList = new ArrayList<>();

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(CARDS_TABLE_NAME);
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = qb.query(db, projectionIn, selection, selectionArgs,
                groupBy, having, sortOrder);

        if(cursor.moveToFirst())
            do {
                int rank = cursor.getInt(cursor.getColumnIndex("rank"));
                String name = cursor.getString(cursor.getColumnIndex("name"));
                String background = cursor.getString(cursor.getColumnIndex("background"));
                String position = cursor.getString(cursor.getColumnIndex("position"));
                int nationalityId = cursor.getInt(cursor.getColumnIndex("nationality_id"));
                int clubId = cursor.getInt(cursor.getColumnIndex("club_id"));
                String picture = cursor.getString(cursor.getColumnIndex("picture_path"));
                int[] attributes = new int[6];
                attributes[0] = cursor.getInt(cursor.getColumnIndex("attr_pac"));
                attributes[1] = cursor.getInt(cursor.getColumnIndex("attr_sho"));
                attributes[2] = cursor.getInt(cursor.getColumnIndex("attr_pas"));
                attributes[3] = cursor.getInt(cursor.getColumnIndex("attr_dri"));
                attributes[4] = cursor.getInt(cursor.getColumnIndex("attr_def"));
                attributes[5] = cursor.getInt(cursor.getColumnIndex("attr_phy"));

                Card cardObject = new Card(rank, name, background,
                        position, nationalityId, clubId, picture, attributes);
                cardsList.add(cardObject);
            } while (cursor.moveToNext());
        cursor.close();
        return cardsList;
    }
}
