package com.coldsunstudio.futpacksimulator.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is responsible for storing all cards a user has
 */

public class UserDatabase extends SQLiteOpenHelper {

    private static final String TAG = UserDatabase.class.getSimpleName();

    private static final String DATABASE_NAME = "usercards.sqlite";
    private static final int DATABASE_VERSION = 1;

    public UserDatabase(Context context) { super(context, DATABASE_NAME, null, DATABASE_VERSION); }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_CARDS_TABLE = "CREATE TABLE " + UserDatabaseContract.CardEntry.TABLE_NAME + " (" +
                UserDatabaseContract.CardEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                UserDatabaseContract.CardEntry.COLUMN_NAME_ISNEW + " INTEGER NOT NULL," +
                UserDatabaseContract.CardEntry.COLUMN_NAME_RANK + " INTEGER NOT NULL," +
                UserDatabaseContract.CardEntry.COLUMN_NAME_NAME + " TEXT NOT NULL," +
                UserDatabaseContract.CardEntry.COLUMN_NAME_BACKGROUND + " TEXT NOT NULL," +
                UserDatabaseContract.CardEntry.COLUMN_NAME_POSITION + " TEXT NOT NULL," +
                UserDatabaseContract.CardEntry.COLUMN_NAME_NATIONALITYID + " INTEGER NOT NULL," +
                UserDatabaseContract.CardEntry.COLUMN_NAME_CLUBID + " INTEGER NOT NULL," +
                UserDatabaseContract.CardEntry.COLUMN_NAME_PICTUREPATH + " TEXT NOT NULL," +
                UserDatabaseContract.CardEntry.COLUMN_NAME_ATTR1 + " TEXT NOT NULL," +
                UserDatabaseContract.CardEntry.COLUMN_NAME_ATTR2 + " TEXT NOT NULL," +
                UserDatabaseContract.CardEntry.COLUMN_NAME_ATTR3 + " TEXT NOT NULL," +
                UserDatabaseContract.CardEntry.COLUMN_NAME_ATTR4 + " TEXT NOT NULL," +
                UserDatabaseContract.CardEntry.COLUMN_NAME_ATTR5 + " TEXT NOT NULL," +
                UserDatabaseContract.CardEntry.COLUMN_NAME_ATTR6 + " TEXT NOT NULL," +
                " UNIQUE (" +
                UserDatabaseContract.CardEntry.COLUMN_NAME_RANK + ", " +
                UserDatabaseContract.CardEntry.COLUMN_NAME_NAME + ", " +
                UserDatabaseContract.CardEntry.COLUMN_NAME_POSITION + " ));";
        Log.v(TAG, SQL_CREATE_CARDS_TABLE);
        db.execSQL(SQL_CREATE_CARDS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) { }

    public List<Card> getAllCards() {

        final String[] SELECT_ALL_CARD_FIELDS = {"0 _id", "is_new", "rank", "name", "background",
                "position", "nationality_id", "club_id", "picture_path",
                "attr_pac", "attr_sho", "attr_pas", "attr_dri", "attr_def", "attr_phy"};

        List<Card> cardsList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(UserDatabaseContract.CardEntry.TABLE_NAME);
        Cursor cursor = qb.query(db, SELECT_ALL_CARD_FIELDS, null, null, null, null, null);

        if(cursor.moveToFirst())
            do {
                int isNew = cursor.getInt(cursor.getColumnIndex(
                        UserDatabaseContract.CardEntry.COLUMN_NAME_ISNEW));
                int rank = cursor.getInt(cursor.getColumnIndex(
                        UserDatabaseContract.CardEntry.COLUMN_NAME_RANK));
                String name = cursor.getString(cursor.getColumnIndex(
                        UserDatabaseContract.CardEntry.COLUMN_NAME_NAME));
                String background = cursor.getString(cursor.getColumnIndex(
                        UserDatabaseContract.CardEntry.COLUMN_NAME_BACKGROUND));
                String position = cursor.getString(cursor.getColumnIndex(
                        UserDatabaseContract.CardEntry.COLUMN_NAME_POSITION));
                int nationalityId = cursor.getInt(cursor.getColumnIndex(
                        UserDatabaseContract.CardEntry.COLUMN_NAME_NATIONALITYID));
                int clubId = cursor.getInt(cursor.getColumnIndex(
                        UserDatabaseContract.CardEntry.COLUMN_NAME_CLUBID));
                String picture = cursor.getString(cursor.getColumnIndex(
                        UserDatabaseContract.CardEntry.COLUMN_NAME_PICTUREPATH));
                int[] attributes = new int[6];
                attributes[0] = cursor.getInt(cursor.getColumnIndex(
                        UserDatabaseContract.CardEntry.COLUMN_NAME_ATTR1));
                attributes[1] = cursor.getInt(cursor.getColumnIndex(
                        UserDatabaseContract.CardEntry.COLUMN_NAME_ATTR2));
                attributes[2] = cursor.getInt(cursor.getColumnIndex(
                        UserDatabaseContract.CardEntry.COLUMN_NAME_ATTR3));
                attributes[3] = cursor.getInt(cursor.getColumnIndex(
                        UserDatabaseContract.CardEntry.COLUMN_NAME_ATTR4));
                attributes[4] = cursor.getInt(cursor.getColumnIndex(
                        UserDatabaseContract.CardEntry.COLUMN_NAME_ATTR5));
                attributes[5] = cursor.getInt(cursor.getColumnIndex(
                        UserDatabaseContract.CardEntry.COLUMN_NAME_ATTR6));

                Card cardObject = new Card(rank, name, background,
                        position, nationalityId, clubId, picture, attributes);
                cardObject.isNew = isNew != 0;
                cardsList.add(cardObject);
            } while (cursor.moveToNext());

        cursor.close();
        db.close();

        return cardsList;
    }

    public void insertCards(List<Card> cardList) {
        ContentValues contentValues = new ContentValues();
        SQLiteDatabase db = getWritableDatabase();

        // update all records to set they are not new anymore:
        contentValues.put(UserDatabaseContract.CardEntry.COLUMN_NAME_ISNEW, 0);
        db.update(UserDatabaseContract.CardEntry.TABLE_NAME, contentValues, null, null);
        contentValues.clear();

        // insert new cards (note: they are new):
        for (Card card : cardList) {
            int[] attributes = card.getAttributes();
            contentValues.put(UserDatabaseContract.CardEntry.COLUMN_NAME_RANK, card.getRank());
            contentValues.put(UserDatabaseContract.CardEntry.COLUMN_NAME_NAME, card.getName());
            contentValues.put(UserDatabaseContract.CardEntry.COLUMN_NAME_BACKGROUND, card.getBackgroundFileName());
            contentValues.put(UserDatabaseContract.CardEntry.COLUMN_NAME_NATIONALITYID, card.getNationalityId());
            contentValues.put(UserDatabaseContract.CardEntry.COLUMN_NAME_CLUBID, card.getClubId());
            contentValues.put(UserDatabaseContract.CardEntry.COLUMN_NAME_POSITION, card.getPosition());
            contentValues.put(UserDatabaseContract.CardEntry.COLUMN_NAME_PICTUREPATH, card.getPictureFileName());
            contentValues.put(UserDatabaseContract.CardEntry.COLUMN_NAME_ISNEW, 1);
            contentValues.put(UserDatabaseContract.CardEntry.COLUMN_NAME_ATTR1, attributes[0]);
            contentValues.put(UserDatabaseContract.CardEntry.COLUMN_NAME_ATTR2, attributes[1]);
            contentValues.put(UserDatabaseContract.CardEntry.COLUMN_NAME_ATTR3, attributes[2]);
            contentValues.put(UserDatabaseContract.CardEntry.COLUMN_NAME_ATTR4, attributes[3]);
            contentValues.put(UserDatabaseContract.CardEntry.COLUMN_NAME_ATTR5, attributes[4]);
            contentValues.put(UserDatabaseContract.CardEntry.COLUMN_NAME_ATTR6, attributes[5]);

            try {
                db.insertOrThrow(UserDatabaseContract.CardEntry.TABLE_NAME, null, contentValues);
            }
            catch(android.database.sqlite.SQLiteConstraintException e) {
                Log.v(TAG, "User already has the received card: " + card.getName() + " " + card.getRank());
            }
        }

        db.close();
    }
}
