package com.coldsunstudio.futpacksimulator.data;

import com.coldsunstudio.futpacksimulator.R;

import java.io.Serializable;

/**
 * Created by damianxb3 on 19.03.2016.
 * Class describing a card with a football player
 */
public class Card implements Serializable {

    public enum CardPack {
        PREMIUM_GOLD,
        GOLD,
        PREMIUM_SILVER,
        SILVER,
        PREMIUM_BRONZE,
        BRONZE
    }

    public enum Color {
        GOLD,
        SILVER,
        BRONZE
    }

    public boolean isNew;
    private int rank;
    private String name;
    private String backgroundFileName;
    private String position;
    private int nationalityId;
    private int clubId;
    private String pictureFileName;
    private int[] attributes;

    public Card(int rank, String name, String backgroundFileName, String position,
                int nationalityId, int clubId, String pictureFileName, int[] attributes) {
        this.rank = rank;
        this.name = name;
        this.backgroundFileName = backgroundFileName;
        this.position = position;
        this.nationalityId = nationalityId;
        this.clubId = clubId;
        this.pictureFileName = pictureFileName;
        this.attributes = attributes;
    }

    public boolean isRare() { return !backgroundFileName.contains("nonrare"); }
    public Color getColor() {
        if(rank >= 75)
            return Color.GOLD;
        if(rank >= 65)
            return Color.SILVER;
        return Color.BRONZE;
    }
    public int getRank() { return rank; }
    public String getName() { return name; }
    public String getBackgroundFileName() { return backgroundFileName; }
    public String getPosition() { return position; }
    public int getNationalityId() { return nationalityId; }
    public int getClubId() { return clubId; }
    public String getPictureFileName() { return pictureFileName; }
    public int[] getAttributes() { return attributes; }

    public static int[] getTextColors(String backgroundFileName) {
        switch (backgroundFileName) {
            case "gold-nonrare":
                return new int[] {R.color.colorTextGoldNonRare, R.color.colorTextGoldNonRare};
            case "goldrare":
                return new int[] {R.color.colorTextGoldRare, R.color.colorTextGoldRare};
            case "if-gold":
                return new int[] {R.color.colorTextGoldInForm, R.color.colorTextGoldInFormAttr};
            case "motm":
                return new int[] {R.color.colorTextMotm, R.color.colorTextMotm};
            case "rb":
                return new int[] {R.color.colorTextRb, R.color.colorTextRb};
            case "hero":
                return new int[] {R.color.colorTextHero, R.color.colorTextHero};
            case "toty":
                return new int[] {R.color.colorTextToty, R.color.colorTextToty};
            case "tots-gold":
                return new int[] {R.color.colorTextTotsGold, R.color.colorTextTotsGold};
            case "tots-silver":
                return new int[] {R.color.colorTextTotsSilver, R.color.colorTextTotsSilver};
            case "tots-bronze":
                return new int[] {R.color.colorTextTotsBronze, R.color.colorTextTotsBronze};
            case "silver-nonrare":
                return new int[] {R.color.colorTextSilverNonRare, R.color.colorTextSilverNonRare};
            case "silverrare":
                return new int[] {R.color.colorTextSilverRare, R.color.colorTextSilverRare};
            case "if-silver":
                return new int[] {R.color.colorTextGoldInForm, R.color.colorTextSilverInFormAttr};
            case "bronze-nonrare":
                return new int[] {R.color.colorTextBronzeNonRare, R.color.colorTextBronzeNonRare};
            case "bronzerare":
                return new int[] {R.color.colorTextBronzeRare, R.color.colorTextBronzeRare};
            case "if-bronze":
                return new int[] {R.color.colorTextGoldInForm, R.color.colorTextBronzeInForm};
            default:
                return new int[] {R.color.colorTextBronzeRare, R.color.colorTextBronzeRare};
        }
    }

}
